//
//  EditProfileViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/8/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FirebaseDatabase
import FirebaseAuth
import SVProgressHUD

class EditProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    var state: Semafor.userStateLogin = .fromSignIn
    var saveState: Semafor.userStateSaveButton = .placedHomeAnnotation
    let locationManager = LocationService.shared.locationManager
    
    fileprivate let firebaseService = FirebaseService.shared
    
    fileprivate lazy var swipeDownRecognizer: UISwipeGestureRecognizer = {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture(gesture:)))
        swipeGesture.direction = .down
        return swipeGesture
    }()
    fileprivate lazy var tapRecognizer: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(imgChangePictureTapped(sender:)))
    }()
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgViewProfilePicture: UIImageView!
    @IBOutlet weak var imgViewChangeProfilePicture: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var userHomeMap: MKMapView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBAction func btnCloseTapped(_ sender: UIButton!){
        showAlertWithCancel(alertTitle: "Exit", alertMessage: "Discard unsaved changes ?", cancelHandler: nil) { _ in
           self.closeEditProfile()
        }
    }
    @IBAction func btnSaveTapped(_ sender: UIButton!){
        guard let firstName = txtFirstName.text, !firstName.isEmpty else {
            showAlert(alertTitle: "Error", alertMessage: "First name can't be empty.")
            return
        }
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let userHomeAnnotation = self.userHomeMap.annotations[0].coordinate
            self.firebaseService.saveUserProfile(firstName: firstName, lastName: self.txtLastName.text!,
                                            picture:  self.imgViewProfilePicture.image!,
                                            lat: userHomeAnnotation.latitude, lng: userHomeAnnotation.longitude){
                self.showAlert(alertTitle: "Success", alertMessage: "Profile updated.", okText: "Ok", okHandler: { _ in
                    self.closeEditProfile()
                })
            }
        }
    }
    
    func closeEditProfile(){
        toggleViewContainer(isHidden: true){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func configureLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func configureView(){
        viewContainer.transform = CGAffineTransform(translationX: 0, y: view.frame.maxY + 200)
        viewContainer.layer.cornerRadius = 10
        viewContainer.addGestureRecognizer(swipeDownRecognizer)
        
        imgViewProfilePicture.layer.cornerRadius = 50 * imgViewProfilePicture.frame.size.height / 100
        imgViewChangeProfilePicture.addGestureRecognizer(tapRecognizer)
        
        btnClose.isHidden = state == .fromSignUp ? true : false
        btnSave.isEnabled = state == .fromSignIn ? true : false
        btnSave.layer.cornerRadius = 5
    }
    
    @objc func handleSwipeGesture(gesture: UISwipeGestureRecognizer){
        if gesture.direction == .down {
            closeEditProfile()
        }
    }
    
    func configureUserHomeLocation(with state: Semafor.userStateLogin){
        var initialLocation = CLLocation()
        
        switch state {
        case .fromSignUp:
            initialLocation = CLLocation(latitude: locationManager.latitude, longitude: locationManager.longitude)
        case .fromSignIn:
            firebaseService.fetchUserProfile(withHandler: { user in
                self.txtFirstName.text = user.firstName
                self.txtLastName.text = user.lastName
                self.imgViewProfilePicture.image = user.profile
                
                initialLocation = CLLocation(latitude: user.lat, longitude: user.lng)
                let artwork = Artwork(title: "Home", locationName: "My home sweet home", discipline: "House", coordinate: initialLocation.coordinate)
                self.userHomeMap.addAnnotation(artwork)
                
                let coordinateRegiion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate, 500, 500)
                self.userHomeMap.setRegion(coordinateRegiion, animated: true)
            })
        }
    }
    
    func configureLongPressForAddAnnotation(){
        let longPressToAddHomeAnnotation = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressToAddHomeAnnotation.minimumPressDuration = 0.15
        longPressToAddHomeAnnotation.delaysTouchesBegan = true
        userHomeMap.addGestureRecognizer(longPressToAddHomeAnnotation)
    }
    
    func choosePhotoForProfilePicture(with state: Semafor.userStateChangeProfilePicture){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = state == .camera ? .camera : .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func toggleSaveButon(after perform: Semafor.userStateSaveButton, _ count: Int = 0){
        if state == .fromSignUp{
            if perform == .placedHomeAnnotation {
                guard (txtFirstName.text?.count) != nil else {return}
            }
            btnSave.isEnabled = count > 0 && userHomeMap.annotations.count > 0 ? true : false
        }
    }
    
    @objc func imgChangePictureTapped(sender: UITapGestureRecognizer){
        showActionSheet(alertTitle: "Change your Profile Picture",
                        FirstOptionTitle: "Take Photo", SecondOptionTitle: "Choose From Library",
                        ThirdOptionTitle: "Use Default Image", FourthOptionTitle: nil,
                        FirstOptionHandler: { _ in
                            self.choosePhotoForProfilePicture(with: .camera)
                        }, SecondOptionHandler: { _ in
                            self.choosePhotoForProfilePicture(with: .library)
                        }, ThirdOptionHandler: { _ in
                            self.imgViewProfilePicture.image = UIImage(named: "defaultProfilePicture")
                        })
    }

    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state != .ended{
            let touchPoint = gestureRecognizer.location(in: self.userHomeMap)
            let location = userHomeMap.convert(touchPoint, toCoordinateFrom: self.userHomeMap)
            
            _ = userHomeMap.annotations.count > 0 ? userHomeMap.removeAnnotations(userHomeMap.annotations) : nil
            let initialLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            let artwork = Artwork(title: "Home", locationName: "My home sweet home", discipline: "House", coordinate: initialLocation.coordinate)
            userHomeMap.addAnnotation(artwork)
            
            toggleSaveButon(after: .placedHomeAnnotation)
        }
    }
    
    func toggleViewContainer(isHidden: Bool, handler: (()->())? = nil){
        UIView.animate(withDuration: 0.75, delay: 0.25, usingSpringWithDamping: 1, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.viewContainer.transform = isHidden ? CGAffineTransform(translationX: 0, y: self.view.frame.maxY + 200) : CGAffineTransform.identity
        }) { _ in handler?() }
    }
}

// MARK: - Life Cycle
extension EditProfileViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLocationManager()
        configureView()
        configureUserHomeLocation(with: state)
        configureLongPressForAddAnnotation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        toggleViewContainer(isHidden: false)
    }
}

// MARK: - ImagePickerController Delegate
extension EditProfileViewController: UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {return}
        imgViewProfilePicture.image = image
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - TextField Delegate
extension EditProfileViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        if text.trimmingCharacters(in: .whitespaces).isEmpty{
            textField.text = nil
        }
        toggleSaveButon(after: .addFirstName, text.count)
    }
}

// MARK: - CLLocation Manager Delegate
extension EditProfileViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if state == .fromSignUp {
            let location = locations[0]
            let span = MKCoordinateSpanMake(0.01, 0.01)
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            let region = MKCoordinateRegionMake(myLocation, span)
            
            userHomeMap.setRegion(region, animated: true)
        }
    }
}
