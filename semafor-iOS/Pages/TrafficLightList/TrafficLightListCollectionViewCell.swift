//
//  TrafficLightListCollectionViewCell.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/11/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class TrafficLightListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgIntersectionPicture: UIImageView!
    @IBOutlet weak var lblIntersectionTitle: UILabel!
}
