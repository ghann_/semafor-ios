//
//  TrafficLightListViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/11/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SVProgressHUD

class TrafficLightListViewController: UIViewController{
    
    @IBOutlet weak var tableListTrafficLight: UITableView!
    weak var btnMenu: UIButton? = UIButton()
    var arrOfTrafficName:[String] = []
    let arrOfTrafficImages = ["intersection0", "intersection1", "intersection2"]
    
    func configureSideMenu(){
        let btnBar = UIButton(type: .custom)
        btnBar.setImage(UIImage(named: "Hamburger"), for: .normal)
        btnBar.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        btnBar.addTarget(self, action: #selector(menuTapped(_:)), for: .touchUpInside)
        btnMenu = btnBar
        let barItem = UIBarButtonItem(customView: btnBar)
        
        let widthConstraint = btnBar.widthAnchor.constraint(equalToConstant: 32)
        let heightConstraint = btnBar.heightAnchor.constraint(equalToConstant: 32)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        self.navigationItem.setLeftBarButton(barItem, animated: true)
    }
    
    func catchNotification(notification: Notification) -> Void{
        btnMenu!.animateHamburgerMenu(when: false)
    }
    
    @objc func menuTapped(_ sender: UIButton!){
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
        sender.animateHamburgerMenu(when: true) {
            self.so_containerViewController?.isSideViewControllerPresented = true
        }
    }
    
    func goToDetailTrafficLight(with selectedTraffic: String?, at: Int){
        guard let selected = selectedTraffic else {return}
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailTrafficLight") as! DetailTrafficLightViewController
        vc.passedText = selected
        vc.passedRow = at
        navigationItem.setBackButtonTitle(to: "TRAFFIC LIGHT LIST")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchTraffic(withHandler handler: @escaping (_ TrafficLight: TrafficLight) -> ()){
        Database.database().reference().child("TrafficLightList").queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            
            guard let dict = snapshot.value as? NSDictionary else{return}
            guard let name = dict["name"] as? String
                else{
                    print("Error in traffic light data")
                    return
            }
            
            let trafficLight = TrafficLight(name: name, redTime: 0, greenTime: 0, interval: 0,
                                            latFromEast: 0, lngFromEast: 0,
                                            latFromNorth: 0, lngFromNorth: 0,
                                            latFromWest: 0, lngFromWest: 0,
                                            latFromSouth: 0, lngFromSouth: 0)
            handler(trafficLight)
        })
    }
    func fetchTrafficOnLaunch(){
        SVProgressHUD.show()
        fetchTraffic { trafficLight in
            self.arrOfTrafficName.append(trafficLight.name)
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                DispatchQueue.main.async {
                    self.tableListTrafficLight.reloadData()
                }
            })
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Life Cycle
extension TrafficLightListViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSideMenu()
        Semafor.notification.addObserver(forName: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, queue: nil, using: catchNotification)
        UIApplication.shared.statusBarStyle = .lightContent
        fetchTrafficOnLaunch()
    }
}

// MARK: - TableView Datasource
extension TrafficLightListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfTrafficName.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrafficLightListTableViewCell") as! TrafficLightListTableViewCell
        cell.imgIntersectionPicture.image = UIImage(named: "\(arrOfTrafficImages[indexPath.row])")
        cell.imgIntersectionPicture.layer.cornerRadius = 28
        cell.imgIntersectionPicture.clipsToBounds = true
        cell.lblIntersectionTitle.text = arrOfTrafficName[indexPath.row]
        return cell
    }
}

// MARK: - TableView Delegate
extension TrafficLightListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! TrafficLightListTableViewCell
        goToDetailTrafficLight(with: cell.lblIntersectionTitle.text, at: indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
