//
//  TrafficLightListTableViewCell.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/11/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class TrafficLightListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgIntersectionPicture: UIImageView!
    @IBOutlet weak var lblIntersectionTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
