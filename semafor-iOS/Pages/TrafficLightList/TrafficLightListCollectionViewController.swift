//
//  TrafficLightListCollectionViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/11/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SVProgressHUD

class TrafficLightListCollectionViewController: UIViewController{
    @IBOutlet weak var trafficListCollectionView: UICollectionView!
    weak var btnMenu: UIButton? = UIButton()
    
    var arrOfTrafficName:[String] = []
     let arrOfTrafficImages = ["intersection0", "intersection1", "intersection2"]
    
    func configureSideMenu(){
        let btnBar = UIButton(type: .custom)
        btnBar.setImage(UIImage(named: "Hamburger"), for: .normal)
        btnBar.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        btnBar.addTarget(self, action: #selector(menuTapped(_:)), for: .touchUpInside)
        btnMenu = btnBar
        let barItem = UIBarButtonItem(customView: btnBar)
        
        let widthConstraint = btnBar.widthAnchor.constraint(equalToConstant: 32)
        let heightConstraint = btnBar.heightAnchor.constraint(equalToConstant: 32)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        self.navigationItem.setLeftBarButton(barItem, animated: true)
    }
    
    func catchNotification(notification: Notification) -> Void{
        btnMenu!.animateHamburgerMenu(when: false)
    }
    
    @objc func menuTapped(_ sender: UIButton!){
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
        sender.animateHamburgerMenu(when: true) {
            self.so_containerViewController?.isSideViewControllerPresented = true
        }
    }
    
    func goToDetailTrafficLight(with selectedTraffic: String?, at: Int){
        guard let selected = selectedTraffic else {return}
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailTrafficLight") as! DetailTrafficLightViewController
        vc.passedText = selected
        vc.passedRow = at
        navigationItem.setBackButtonTitle(to: "TRAFFIC LIGHT LIST")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchTraffic(withHandler handler: @escaping (_ TrafficLight: TrafficLight) -> ()){
        Database.database().reference().child("TrafficLightList").queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            
            guard let dict = snapshot.value as? NSDictionary else{return}
            guard let name = dict["name"] as? String
                else{
                    print("Error in traffic light data")
                    return
            }
            
            let trafficLight = TrafficLight(name: name, redTime: 0, greenTime: 0, interval: 0,
                                            latFromEast: 0, lngFromEast: 0,
                                            latFromNorth: 0, lngFromNorth: 0,
                                            latFromWest: 0, lngFromWest: 0,
                                            latFromSouth: 0, lngFromSouth: 0)
            handler(trafficLight)
        })
    }
    func fetchTrafficOnLaunch(){
        SVProgressHUD.show()
        fetchTraffic { trafficLight in
            self.arrOfTrafficName.append(trafficLight.name)
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                DispatchQueue.main.async {
                    self.trafficListCollectionView.reloadData()
                }
            })
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Life Cycle
extension TrafficLightListCollectionViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trafficListCollectionView.contentInset = UIEdgeInsets(top: 11, left: 11, bottom: 11, right: 11)
        configureSideMenu()
        Semafor.notification.addObserver(forName: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, queue: nil, using: catchNotification)
        fetchTrafficOnLaunch()
         UIApplication.shared.statusBarStyle = .lightContent
    }
}

// MARK: - CollectionView Datasource
extension TrafficLightListCollectionViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfTrafficName.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrafficLightListCollectionViewCell", for: indexPath) as! TrafficLightListCollectionViewCell
        cell.imgIntersectionPicture.image = UIImage(named: "\(arrOfTrafficImages[indexPath.row])")
        cell.lblIntersectionTitle.text = arrOfTrafficName[indexPath.row]
        return cell
    }
}

// MARK: - CollectionView Delegate
extension TrafficLightListCollectionViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TrafficLightListCollectionViewCell
        cell.highlightedWhenTapped {
            self.goToDetailTrafficLight(with: cell.lblIntersectionTitle.text, at: indexPath.row)
        }
    }
}

// MARK: - CollectionView FlowLayout Delegate
extension TrafficLightListCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 35
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
    }
}
