//
//  DetailTrafficLightViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/11/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class DetailTrafficLightViewController: UIViewController {
    @IBOutlet weak var imgIntersection: UIImageView!
    var passedText = "ddd"
    var passedRow = 0
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Life Cycle
extension DetailTrafficLightViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = passedText
        self.imgIntersection.image = UIImage(named: "intersection\(passedRow)")
        UIApplication.shared.statusBarStyle = .lightContent
    }
}
