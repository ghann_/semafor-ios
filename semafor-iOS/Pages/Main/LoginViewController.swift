//
//  ViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/24/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import RevealTextField
import SVProgressHUD
import RevealingSplashView
import RealmSwift

class LoginViewController: UIViewController {

    fileprivate let firebaseService = FirebaseService.shared
    fileprivate var state: Semafor.userStateMain = .signIn
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBAction func btnsTapped(_ sender: UIButton) {
        view.endEditing(true)
        switch sender.tag {
        case 0:
            guard let email = txtEmail.text, !email.isEmpty,
                let pass = txtPassword.text, !pass.isEmpty else{ return }
            
            SVProgressHUD.show()
            state == .signIn ? userSignIn(with: email, and: pass) : userSignUp(with: email, and: pass)
        case 1:
            switch state{
            case .signIn:
                sender.setTitle("Already have an account? Sign In!", for: .normal)
                btnLogin.setTitle("SIGN UP", for: .normal)
                state = .signUp
            case .signUp:
                sender.setTitle("Don't have an account? Sign Up!", for: .normal)
                btnLogin.setTitle("SIGN IN", for: .normal)
                state = .signIn
            }
        case 2:
            guard let email = txtEmail.text, !email.isEmpty else{ return }
            showAlertWithCancel(alertTitle: "Confirmation", alertMessage: "Send reset password for \(email) ?"){ _ in
                SVProgressHUD.show()
                self.userForgotPassword(with: email)
            }
        default: ()
        }
    }
    
    func configureRememberMe(with email: String, and password: String, state: Semafor.userStateLogin){
        if state == .fromSignIn {return}
        let rememberUser = RememberMe()
        rememberUser.email = email
        rememberUser.password = password
        rememberUser.isRemembered = false
        
        let realm = try! Realm()
        let rememberCount = realm.objects(RememberMe.self).count
        
        try! realm.write {
            if state == .fromSignUp && rememberCount > 0 {
                realm.objects(RememberMe.self).first?.setValue(email, forKey: "email")
                realm.objects(RememberMe.self).first?.setValue(password, forKey: "password")
                realm.objects(RememberMe.self).first?.setValue(false, forKey: "isRemembered")
            }else
            if state == .fromSignUp && rememberCount == 0 {
                realm.add(rememberUser)
            }
        }
    }
    
    func userSignIn(with email: String, and password: String){
        firebaseService.signInUser(withEmail: email, password: password) { err in
            if let error = err {
                self.showAlert(alertTitle: "Error", alertMessage: error.localizedDescription)
            } else{
                self.configureRememberMe(with: email, and: password, state: .fromSignIn)
                self.userGoSignIn(with: .fromSignIn)
            }
        }
    }
    
    func userSignUp(with email: String, and password: String){
        firebaseService.signUpUSer(withEmail: email, password: password) { err in
            if let error = err {
                 self.showAlert(alertTitle: "Error", alertMessage: error.localizedDescription)
            } else{
                SVProgressHUD.show(withStatus: "You have successfully registered \n Taking you back to Application")
                SVProgressHUD.dismiss(withDelay: 2, completion: {
                    self.configureRememberMe(with: email, and: password, state: .fromSignUp)
                    self.userGoSignIn(with: .fromSignUp)
                })
            }
        }
    }
    
    func userForgotPassword(with email: String){
        firebaseService.userForgotPassword(withEmail: email) { err in
            if let error = err {
                self.showAlert(alertTitle: "Error", alertMessage: error.localizedDescription)
            } else{
                self.showAlert(alertTitle: "Success", alertMessage: "Password reset sent to \(email)")
            }
        }
    }
    
    func userGoSignIn(with option: Semafor.userStateLogin){
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "GoToSideMenu") as? ContainerViewController else {return}
        vc.state = option
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func showSplashScreen(){
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "semaforIcon")!,iconInitialSize: CGSize(width: 70, height: 70), backgroundColor: Semafor.Colors.defaultDarkerBlue)
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        view.addSubview(revealingSplashView)
        revealingSplashView.startAnimation()
    }
    
    func checkForRememberMe(){
        let realm = try! Realm()
        guard let rememberObj = realm.objects(RememberMe.self).first else {return}
        if rememberObj.isRemembered{
            txtEmail.text = rememberObj.email
            txtPassword.text = rememberObj.password
        }
    }
    
    func configureView(){
        showSplashScreen()
        view.addBackground()
        txtPassword.revealable(secureImage: #imageLiteral(resourceName: "eye"), unsecureImage: #imageLiteral(resourceName: "eye-reveal"), tintColor: Semafor.Colors.defaultBlackAlt)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}

// MARK: - Life Cycle
extension LoginViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkForRememberMe()
        configureView()
        Semafor.notification.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        Semafor.notification.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

// MARK: - TextField Handling
extension LoginViewController{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
}

// MARK: - TextField Delegate
extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.returnKeyType {
        case .next:
            _ = txtEmail.isFirstResponder ? txtPassword.becomeFirstResponder() : nil
        case .done:
             textField.resignFirstResponder()
        default: ()
        }
        return true
    }
}
