//
//  RememberMeCell.swift
//  semafor-iOS
//
//  Created by unrealBots on 1/20/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import UIKit

class RememberMeCell: UITableViewCell {

    @IBOutlet weak var switchRememberMe: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
