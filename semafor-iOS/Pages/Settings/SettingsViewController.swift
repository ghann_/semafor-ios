//
//  SettingsViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import RealmSwift

class SettingsViewController: UIViewController {
    
    weak var btnMenu: UIButton? = UIButton()
    
    func configureSideMenu(){
        let btnBar = UIButton(type: .custom)
        btnBar.setImage(UIImage(named: "Hamburger"), for: .normal)
        btnBar.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        btnBar.addTarget(self, action: #selector(menuTapped(_:)), for: .touchUpInside)
        btnMenu = btnBar
        let barItem = UIBarButtonItem(customView: btnBar)
        
        let widthConstraint = btnBar.widthAnchor.constraint(equalToConstant: 32)
        let heightConstraint = btnBar.heightAnchor.constraint(equalToConstant: 32)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        self.navigationItem.setLeftBarButton(barItem, animated: true)
    }
    
    func catchNotification(notification: Notification) -> Void{
        btnMenu!.animateHamburgerMenu(when: false)
    }
    
    func getCurrentRememberMe(handler: (_ rememberMe: RememberMe) -> ()){
        let realm = try! Realm()
        let rememberMe = realm.objects(RememberMe.self)
        handler(rememberMe[0])
    }
    
    @objc func menuTapped(_ sender: UIButton!){
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
        sender.animateHamburgerMenu(when: true) {
            self.so_containerViewController?.isSideViewControllerPresented = true
        }
    }
    
    func changeIsRememberedValue(with state: Bool){
        let realm = try! Realm()
        try! realm.write {
            realm.objects(RememberMe.self).first?.setValue(state, forKey: "isRemembered")
        }
    }
    
    @objc func switchChanged(_ sender: UISwitch!){
        if sender.isOn{
            showAlertWithCancel(alertTitle: "Remember Me", alertMessage: "Enable remember me on Sign in ?", okText: "Enable", cancelText: "Cancel", cancelHandler: { _ in
                UIView.animate(withDuration: 0.25, animations: { sender.isOn = false })
            }, okHandler: { _ in
                self.changeIsRememberedValue(with: true)
            })
        }else{
            showAlertWithCancel(alertTitle: "Remember Me", alertMessage: "Disable remember me on Sign in ?", okText: "Disable", cancelText: "Cancel", cancelHandler: { _ in
                UIView.animate(withDuration: 0.25, animations: { sender.isOn = true })
            }, okHandler: { _ in
                self.changeIsRememberedValue(with: false)
            })
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - Life Cycle
extension SettingsViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .lightContent
        configureSideMenu()
        Semafor.notification.addObserver(forName: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, queue: nil, using: catchNotification)
    }
}

// MARK: - TableView Datasource
extension SettingsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RememberMeCell", for: indexPath) as! RememberMeCell
        
        getCurrentRememberMe { rememberMe in
            cell.switchRememberMe.isOn = rememberMe.isRemembered
        }
        
        cell.switchRememberMe.addTarget(self, action: #selector(switchChanged(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - TableView Delegate
extension SettingsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
