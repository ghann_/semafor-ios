//
//  ConntainerViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import SidebarOverlay

class ContainerViewController: SOContainerViewController {

    var state: Semafor.userStateLogin = .fromSignIn
    
    fileprivate lazy var homeViewController: UIViewController = {
        let homeVC = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as! HomeViewController
        homeVC.state = self.state
        return homeVC
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SOdelegate = self
        topViewController = homeViewController
        sideViewController = storyboard?.instantiateViewController(withIdentifier: "LeftMenu")
        menuSide = .left
        sideMenuWidth = 75 * UIScreen.main.bounds.width / 100
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
}

// MARK: - SODelegate
extension ContainerViewController: SOContainerViewDelegate {
    func didTapContentCover() {
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
    }
}
