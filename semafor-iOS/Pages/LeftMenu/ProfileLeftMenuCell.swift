//
//  ProfileLeftMenuCell.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/30/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class ProfileLeftMenuCell: UITableViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLastLogin: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    
}
