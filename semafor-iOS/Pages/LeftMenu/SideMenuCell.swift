//
//  SideMenuCell.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/3/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var imgMenuIcon: UIImageView!
    @IBOutlet weak var lblMenuTitle: UILabel!
    
}
