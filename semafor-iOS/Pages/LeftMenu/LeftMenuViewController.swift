//
//  LeftMenuViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import AZImagePreview

class LeftMenuViewController: UIViewController {

    fileprivate let menuItems = ["Home", "Traffic Light List", "Settings"]
    fileprivate let menuIcons = ["home", "trafficList", "settings"]
    fileprivate let firebaseService = FirebaseService.shared
    
    fileprivate lazy var loginViewController: UIViewController = {
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        loginVC?.modalTransitionStyle = .crossDissolve
        return loginVC!
    }()
    fileprivate lazy var editProfileViewController: UIViewController = {
        let editVC = UIStoryboard(name: "EditProfile", bundle: nil).instantiateInitialViewController() as! EditProfileViewController
        editVC.state = .fromSignIn
        editVC.modalTransitionStyle = .crossDissolve
        editVC.modalPresentationStyle = .overCurrentContext
        return editVC
    }()
    fileprivate lazy var homeViewController: UIViewController = {
        return storyboard!.instantiateViewController(withIdentifier: "GoToHome")
    }()
    fileprivate lazy var trafficListViewController: UIViewController = {
        return storyboard!.instantiateViewController(withIdentifier: "GoToTrafficLightList")
    }()
    fileprivate lazy var settingsViewController: UIViewController = {
        return storyboard!.instantiateViewController(withIdentifier: "GoToSettings")
    }()
    
    var user: UserInfo? = nil
    @IBOutlet weak var leftMenuTable: UITableView!
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBAction func btnMenuTapped(_ sender: UIButton!){
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
        sender.animateHamburgerMenu(when: false) {
            self.so_containerViewController?.isSideViewControllerPresented = false
        }
    }
    @IBAction func btnSignOutTapped(_ sender: UIButton!){
        showAlertWithCancel(alertTitle: "Confirmation", alertMessage: "Are you sure want to sign out ?"){ _ in
            FirebaseService.shared.signOutCurrentUser()
            self.present(self.loginViewController, animated: true, completion: nil)
        }
    }
    
    func catchNotifications(notification: Notification) -> Void{
        btnMenu.animateHamburgerMenu(when: true)
    }
    
    @objc func btnEditProfileTapped(_ sender: UIButton){
        btnMenu.sendActions(for: .touchUpInside)
        present(editProfileViewController, animated: true, completion: nil)
    }
    
    func getCurrentTime() -> String{
        let hour = Int(Calendar.current.component(.hour, from: Date()))
        var state = ""
        
        switch hour {
        case 0...10:  state = "Morning"
        case 11...17: state = "Afternoon"
        case 18...24: state = "Evening"
        default: ()
        }
        return "Good \(state) !"
    }
    
    func fetchProfile(){
        firebaseService.fetchUserProfile { user in
            self.user = user
            self.leftMenuTable.reloadData()
        }
    }
}

// MARK: - Life Cycle
extension LeftMenuViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchProfile()
        btnMenu.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        Semafor.notification.addObserver(forName: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, queue: nil, using: catchNotifications)
    }
}

// MARK: - TableView DataSource
extension LeftMenuViewController: UITableViewDataSource, ProfileCell{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileLeftMenuCell", for: indexPath) as! ProfileLeftMenuCell
            configureCell(forCell: cell)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
            cell.lblMenuTitle.text = menuItems[indexPath.row - 1]
            cell.imgMenuIcon.image = UIImage(named: menuIcons[indexPath.row - 1])
            return cell
        }
    }
    
    func configureCell(forCell cell: ProfileLeftMenuCell) {
        cell.selectionStyle = .none
        cell.imgProfile.image = user?.profile
        cell.imgProfile.layer.cornerRadius = 50 * cell.imgProfile.frame.width / 100
        cell.imgProfile.layer.borderColor = Semafor.Colors.defaultBlue.cgColor
        cell.imgProfile.layer.borderWidth = 2.5
        cell.imgProfile.clipsToBounds = true
        cell.imgProfile.delegate = self
        
        if let firstName = user?.firstName{
            if let lastName = user?.lastName{
             cell.lblUserName.text = "\(firstName) \(lastName)"
            } else{
                cell.lblUserName.text = "\(firstName)"
            }
        }
        cell.lblLastLogin.text = getCurrentTime()
        cell.btnEditProfile.addTarget(self, action: #selector(btnEditProfileTapped(_:)), for: .touchUpInside)
    }
}

// MARK: - TableView Delegate
extension LeftMenuViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var vc = UIViewController()
        switch indexPath.row {
        case 1: vc = homeViewController
        case 2: vc = trafficListViewController
        case 3: vc = settingsViewController
        default: return
        }
        
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = false
            self.so_containerViewController?.topViewController = vc
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return 100
        default: return 35
        }
    }
}

// MARK: - AZImagePreview Delegate
extension LeftMenuViewController: AZPreviewImageViewDelegate{
    func previewImageViewInRespectTo(_ previewImageView: UIImageView) -> UIView? {
        return view
    }
    
    func previewImageView(_ previewImageView: UIImageView, requestImagePreviewWithPreseneter presenter: AZImagePresenterViewController) {
        present(presenter, animated: false, completion: nil)
    }
}

protocol ProfileCell{
    func configureCell(forCell: ProfileLeftMenuCell)
}
