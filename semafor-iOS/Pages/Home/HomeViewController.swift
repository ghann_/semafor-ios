//
//  HomeViewController.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FirebaseDatabase
import SVProgressHUD
import UserNotifications

class HomeViewController: UIViewController {
    
    var state: Semafor.userStateLogin = .fromSignIn
    var annotationState: Semafor.userStateAnnotation = .traffic
    var trafficLightState: Semafor.stateTrafficLight = .red
    var tripState: Semafor.userStateTrip = .canceled
    
    let manager = CLLocationManager()
    var timer = Timer()
    var second = 0
    var dateInfo = DateComponents()
    
    var arrOfTrafficLight:[TrafficLight] = []
    var arrOfAnnotation:[MKAnnotation] = []
    var arrOfSelectedTrafficLight:[TrafficLight] = []
    
    var currentUserLocation: CLLocationCoordinate2D {
        let lat = self.manager.location?.coordinate.latitude
        let lng = self.manager.location?.coordinate.longitude
        let location = CLLocationCoordinate2DMake(lat!, lng!)
        return location
    }
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var viewStartTripButton: UIView!
    @IBOutlet weak var btnStartTrip: UIButton!
    @IBOutlet weak var constStartTripButton: NSLayoutConstraint!
    @IBOutlet weak var mainViewMapKit: MKMapView!
    @IBAction func btnOpeMenuTapped(_ sender: UIButton!){
        Semafor.notification.post(name: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, userInfo: nil)
        sender.animateHamburgerMenu(when: true) {
            self.so_containerViewController?.isSideViewControllerPresented = true
        }
    }
    @IBAction func btnZoomTapped(_ sender: UIButton){
        sender.animateZoomButton()
        switch sender.tag {
        case 0:
           // let region: MKCoordinateRegion = MKCoordinateRegionMake(currentUserLocation, MKCoordinateSpanMake(0.0025, 0.0025))
           // mainViewMapKit.setRegion(region, animated: true)
            
            break
        case 1:
            //let region: MKCoordinateRegion = MKCoordinateRegionMake(currentUserLocation, MKCoordinateSpanMake(0.01, 0.01))
            //mainViewMapKit.setRegion(region, animated: true)
            break
        default:
            break
        }
    }
    @IBAction func btnStartTripTapped(_ sender: UIButton){
        if tripState == .onGoing{
            second = arrOfTrafficLight[0].redTime
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            btnStartTrip.setTitle("CANCEL TRIP", for: .normal)
            tripState = .canceled
        }else{
            showAlertWithCancel(alertTitle: "Cancel Trip", alertMessage: "Are you sure want to cancel on going trip?", okText: "Ok", cancelText: "cancel", cancelHandler: nil, okHandler: { _ in
                self.timer.invalidate()
                self.mainViewMapKit.removeOverlays(self.mainViewMapKit.overlays)
                self.mainViewMapKit.removeAnnotations(self.mainViewMapKit.annotations)
                self.mainViewMapKit.addAnnotations(self.arrOfAnnotation)
                self.removeUserPlacedPin()
                self.btnStartTrip.setTitle("START TRIP", for: .normal)
                self.hideStartTripButton()
                self.tripState = .onGoing
            })
        }
    }
    func setUpInitialLocation(){
//        let initialLocation = CLLocation(latitude: currentUserLocation.latitude, longitude: currentUserLocation.longitude)
//        let radius: CLLocationDistance = 500
//        let coordinateRegiion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate, radius, radius)
//        mainViewMapKit.setRegion(coordinateRegiion, animated: true)
    }
    
    func catchNotification(notification: Notification) -> Void{
        btnMenu.animateHamburgerMenu(when: false)
    }
    
    func removeAnnotation(second: Int){
        if second == 60 {
            for annotation in mainViewMapKit.annotations{
                if annotation.title.unsafelyUnwrapped == "Home0" { mainViewMapKit.removeAnnotation(annotation) }
            }
        }else{
            let title = trafficLightState == .red ? "Green Light in \(second) seconds" : "Red Light in \(second) seconds"
            for annotation in mainViewMapKit.annotations{
                if annotation.title.unsafelyUnwrapped == title { mainViewMapKit.removeAnnotation(annotation) }
            }
        }
    }
    func selectAnnotation(second: Int){
        for annotation in mainViewMapKit.annotations{
            let title = trafficLightState == .red ? "Green Light in \(second) seconds" : "Red Light in \(second) seconds"
            if annotation.title.unsafelyUnwrapped == title {
                mainViewMapKit.selectAnnotation(annotation, animated: false)
                break
            }
        }
    }
    
    @objc func updateTimer() {
        removeAnnotation(second: second)
        
        if second == 0{
            trafficLightState = trafficLightState == .red ? .green : .red
            second = trafficLightState == .red ? arrOfTrafficLight[0].redTime : arrOfTrafficLight[0].greenTime
        }
        second -= 1
        
        let title = "Hurry Up!"
        var body = ""
        if second == 11 {
            dateInfo.hour = Calendar.current.component(.hour, from: Date())
            dateInfo.minute = Calendar.current.component(.minute, from: Date())
            dateInfo.second = Calendar.current.component(.second, from: Date()) + 1
                
            body = trafficLightState == .red ? "\(arrOfTrafficLight[0].name) Intersection Green Light in 10 seconds." : "\(arrOfTrafficLight[0].name) Intersection Red Light in 10 seconds."
            configureLocalNotification(dateInfo: dateInfo, title: title, body: body)
        }
       
        let locCoord = CLLocation(latitude: arrOfTrafficLight[0].latFromEast, longitude: arrOfTrafficLight[0].lngFromEast)
        if trafficLightState == .red {
            let artwork = Artwork(title: "Green Light in \(second) seconds", locationName: "\( arrOfTrafficLight[0].name)", discipline: "Merah", coordinate: locCoord.coordinate)
            mainViewMapKit.addAnnotation(artwork)
        }else{
            let artwork = Artwork(title: "Red Light in \(second) seconds", locationName:"\( arrOfTrafficLight[0].name)", discipline: "Hijau", coordinate: locCoord.coordinate)
            mainViewMapKit.addAnnotation(artwork)
        }
        
        selectAnnotation(second: second)
    }
    
    func getDirection(lat: Double, lng: Double){
        let sourceLocation = CLLocationCoordinate2D(latitude: currentUserLocation.latitude, longitude: currentUserLocation.longitude)
        let destinationLocation = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate { response, error -> Void in
            guard let response = response else {
                if let error = error { print("Error: \(error)") }
                return
            }
            
            if let route = response.routes.first {
                let coordinates: [CLLocationCoordinate2D] = {
                    var array = [CLLocationCoordinate2D]()
                    let polyline = route.polyline
                    for i in 0 ..< polyline.pointCount {
                        
                        let point = polyline.points()[i]
                        array.append(MKCoordinateForMapPoint(point))
                        
                        //print("\(i) \(MKCoordinateForMapPoint(point))")
                        let coordinatePoints = MKCoordinateForMapPoint(point)
                        for j in 0 ..< self.arrOfTrafficLight.count{
                            if self.arrOfTrafficLight[j].latFromEast == coordinatePoints.latitude && self.arrOfTrafficLight[j].lngFromEast == coordinatePoints.longitude{
                                self.removeAllTrafficLightAnnotationsExceptPlaced()
                                
                                let locCoord = CLLocation(latitude: self.arrOfTrafficLight[j].latFromEast, longitude: self.arrOfTrafficLight[j].lngFromEast)
                                let artwork = Artwork(title: "Home", locationName: self.arrOfTrafficLight[j].name, discipline: "House", coordinate: locCoord.coordinate)
                                self.arrOfAnnotation.append(artwork)
                                self.mainViewMapKit.addAnnotation(artwork)
                                
                                print("matched with \(self.arrOfTrafficLight[j].name)")
                            }
                        }
                        
                        let circlePoint = MKCoordinateForMapPoint(point)
                        self.mainViewMapKit.add(MKCircle(center: circlePoint, radius: 2))
                    }
                    return array
                }()
                
                //print(coordinates)
            }
            
            let route = response.routes[0]
            self.mainViewMapKit.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mainViewMapKit.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }

    func addTrafficToFirebase(){
        let firebaseRef = Database.database().reference()
        
        let traffic = [
            "name"             : "Nanyang School Intersection",
            "redTime"          : 124,
            "greenTime"        : 23,
            "interval"         : 147,
            "latFromEast"      : -6.289267,
            "lngFromEast"      : 106.64968,
            "latFromNorth"     : 0,
            "lngFromNorth"     : 0,
            "latFromWest"      : 0,
            "lngFromWest"      : 0,
            "latFromSouth"     : 0,
            "lngFromSouth"     : 0
            ] as [String : Any]
        
        firebaseRef.child("TrafficLightList").childByAutoId().setValue(traffic)
    }
    func fetchTraffic(withHandler handler: @escaping (_ TrafficLight: TrafficLight) -> ()){
        Database.database().reference().child("TrafficLightList").queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            
            guard let dict = snapshot.value as? NSDictionary else{return}
            guard let name      = dict["name"] as? String,
                  let redTime   = dict["redTime"] as? Int,
                  let greenTime = dict["greenTime"] as? Int,
                  let interval  = dict["interval"] as? Int,
                  let latFromEast = dict["latFromEast"] as? Double,
                  let lngFromEast = dict["lngFromEast"] as? Double,
                  let latFromNorth = dict["latFromNorth"] as? Double,
                  let lngFromNorth = dict["lngFromNorth"] as? Double,
                  let latFromWest = dict["latFromWest"] as? Double,
                  let lngFromWest = dict["lngFromWest"] as? Double,
                  let latFromSouth = dict["latFromSouth"] as? Double,
                  let lngFromSouth = dict["lngFromSouth"] as? Double
            else{
                print("Error in traffic light data")
                return
            }

            let trafficLight = TrafficLight(name: name, redTime: redTime, greenTime: greenTime, interval: interval,
                                            latFromEast: latFromEast, lngFromEast: lngFromEast,
                                            latFromNorth: latFromNorth, lngFromNorth: lngFromNorth,
                                            latFromWest: latFromWest, lngFromWest: lngFromWest,
                                            latFromSouth: latFromSouth, lngFromSouth: lngFromSouth)
            handler(trafficLight)
        })
    }
    func fetchTrafficOnLaunch(){
        SVProgressHUD.show()
        fetchTraffic { trafficLight in
            let locCoord = CLLocation(latitude: trafficLight.latFromEast, longitude: trafficLight.lngFromEast)
            let artwork = Artwork(title: "Home", locationName: trafficLight.name, discipline: "House", coordinate: locCoord.coordinate)
            self.arrOfTrafficLight.append(trafficLight)
            self.arrOfAnnotation.append(artwork)
            SVProgressHUD.dismiss(withDelay: 0.5, completion: {
                self.mainViewMapKit.addAnnotations(self.arrOfAnnotation)
            })
        }
    }

    func configureLongPressForAddAnnotation(){
        let longPressToAddHomeAnnotation = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressToAddHomeAnnotation.minimumPressDuration = 0.75
        longPressToAddHomeAnnotation.delaysTouchesBegan = true
        mainViewMapKit.addGestureRecognizer(longPressToAddHomeAnnotation)
    }
    
    func removeUserPlacedPin(){
        for annotation in mainViewMapKit.annotations{
            if !annotation.isKind(of: MKUserLocation.self){
                guard let ann = annotation as? Artwork else {return}
                guard let annName = ann.title else {return}
                if annName == "PlacedPin" {
                    mainViewMapKit.removeAnnotation(annotation)
                    break
                }
            }
        }
    }
    
    func removeAllTrafficLightAnnotationsExceptPlaced(){
        for annotation in mainViewMapKit.annotations{
            if !annotation.isKind(of: MKUserLocation.self){
                guard let ann = annotation as? Artwork else {return}
                guard let annName = ann.title else {return}
                if annName != "PlacedPin" {
                    mainViewMapKit.removeAnnotation(annotation)
                }
            }
        }
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began{
            SVProgressHUD.show()
            
            let touchPoint = gestureRecognizer.location(in: self.mainViewMapKit)
            let location = self.mainViewMapKit.convert(touchPoint, toCoordinateFrom: self.mainViewMapKit)
            
            annotationState = .placed
            
            removeUserPlacedPin()
            let initialLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            let artwork = Artwork(title: "PlacedPin", locationName: "PlacedPin", discipline: "Other", coordinate: initialLocation.coordinate)
            self.mainViewMapKit.addAnnotation(artwork)
            
            handleDestinationOnLongPress(lat: location.latitude, lng: location.longitude)
        }
    }
    func handleDestinationOnLongPress(lat: Double, lng: Double){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: lng)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let locationName = placemarks?[0].name,
                  let city         = placemarks?[0].subLocality,
                  let street       = placemarks?[0].country else{return}
            
            SVProgressHUD.dismiss()
            self.showAlertWithCancel(alertTitle: "Destination", alertMessage: "Go to \(locationName), \(city), \(street)?", okText: "Ok", cancelText: "Cancel", cancelHandler: { _ in
                self.mainViewMapKit.removeOverlays(self.mainViewMapKit.overlays)
                self.removeUserPlacedPin()
                self.hideStartTripButton()
            }, okHandler: { _ in
                self.mainViewMapKit.removeOverlays(self.mainViewMapKit.overlays)
                self.getDirection(lat: lat, lng: lng)
                
                self.showStartTripButton()
                self.tripState = .onGoing
            })
        })
    }
    
    func showStartTripButton(){
        self.constStartTripButton.constant = 20
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.viewStartTripButton.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func hideStartTripButton(){
        self.constStartTripButton.constant = -200
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.viewStartTripButton.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func configureLocalNotification(dateInfo: DateComponents, title: String, body: String){
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.scheduleNotification(dateInfo: dateInfo, title: title, body: body)
    }
}

// MARK: - Life Cycle
extension HomeViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UNUserNotificationCenter.current().delegate = self
        UIApplication.shared.statusBarStyle = .default
        
        configureLongPressForAddAnnotation()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        setUpInitialLocation()
        
        Semafor.notification.addObserver(forName: NSNotification.Name(rawValue: Semafor.notificationKey), object: nil, queue: nil, using: catchNotification)
        
        hideStartTripButton()
    }   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if state == .fromSignUp {
            fetchTrafficOnLaunch()
            guard let editVC = UIStoryboard(name: "EditProfile", bundle: nil).instantiateInitialViewController() as? EditProfileViewController else {return}
            editVC.state = .fromSignUp
            editVC.modalTransitionStyle = .crossDissolve
            editVC.modalPresentationStyle = .overCurrentContext
            self.present(editVC, animated: true, completion: nil)
        }else{
            fetchTrafficOnLaunch()
        }
    }
}

// MARK: - MapView Delegate
extension HomeViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{ return nil }
        
        var annotationView = self.mainViewMapKit.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = true
        }else{
            annotationView?.annotation = annotation
        }
        
        var annotationImage = UIImage(named: "pinPlaced")
        
        if annotationState == .traffic{
            if trafficLightState == .green{
                annotationImage = second <= 11 ? UIImage(named: "yellowLightPin") : UIImage(named: "greenLightPin")
            }else{
                annotationImage = second <= 11 ? UIImage(named: "yellowLightPin") : UIImage(named: "redLightPin")
            }
        }
        
        annotationView?.image = annotationImage
        annotationView?.frame.size = annotationState == .traffic ? CGSize(width: 50, height: 117) : CGSize(width: 25, height: 57)
        annotationState = .traffic
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor(red:0.11, green:0.24, blue:0.44, alpha:0.75)
            renderer.lineWidth = 3.0
            
            return renderer
        }else
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: circleOverlay)
            circleRenderer.fillColor = Semafor.Colors.defaultBlack
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
}

// MARK: - CLLocationManager Delegate
extension HomeViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let region = MKCoordinateRegionMake(myLocation, span)
        
        //Uncomment to set always on location
        //mainViewMapKit.setRegion(region, animated: true)
        mainViewMapKit.showsUserLocation = true
    }
}

// MARK: - UNUserNotificationCenter Delegate
extension HomeViewController: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
