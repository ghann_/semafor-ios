//
//  TrafficLight.swift
//  semafor-iOS
//
//  Created by unrealBots on 1/19/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation

class TrafficLight {
    var name: String
    var redTime: Int
    var greenTime: Int
    var interval: Int
    var latFromEast: Double
    var lngFromEast: Double
    var latFromNorth: Double
    var lngFromNorth: Double
    var latFromWest: Double
    var lngFromWest: Double
    var latFromSouth: Double
    var lngFromSouth: Double
    
    init(name: String, redTime: Int, greenTime: Int, interval: Int,
         latFromEast: Double,  lngFromEast: Double,
         latFromNorth: Double, lngFromNorth: Double,
         latFromWest: Double,  lngFromWest: Double,
         latFromSouth: Double, lngFromSouth: Double) {
        self.name = name
        self.redTime = redTime
        self.greenTime = greenTime
        self.interval = interval
        self.latFromEast  = latFromEast
        self.lngFromEast  = lngFromEast
        self.latFromNorth = latFromNorth
        self.lngFromNorth = lngFromNorth
        self.latFromWest  = latFromWest
        self.lngFromWest  = lngFromWest
        self.latFromSouth = latFromSouth
        self.lngFromSouth = lngFromSouth
    }
}
