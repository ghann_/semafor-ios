//
//  RememberMe.swift
//  semafor-iOS
//
//  Created by unrealBots on 1/20/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import RealmSwift

class RememberMe: Object{
    @objc dynamic var email    = ""
    @objc dynamic var password = ""
    @objc dynamic var isRemembered = false
}
