//
//  User.swift
//  semafor-iOS
//
//  Created by unrealBots on 1/19/18.
//  Copyright © 2018 unrealBots. All rights reserved.
//

import Foundation
import UIKit

class UserInfo {
    let firstName: String
    let lastName: String?
    let profile: UIImage?
    let lat: Double
    let lng: Double
    
    init(firstName: String, lastName: String?, profile: UIImage?, lat: Double, lng: Double){
        self.firstName = firstName
        self.lastName = lastName
        self.profile = profile
        self.lat = lat
        self.lng = lng
    }
}
