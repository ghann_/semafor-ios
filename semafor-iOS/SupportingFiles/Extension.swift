//
//  Extension.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIView Extension
extension UIView{
    func addBackground(imageName: String = "BackgroundGradient.jpg", contextMode: UIViewContentMode = .scaleToFill) {
        let backgroundImageView = UIImageView(frame: UIScreen.main.bounds)
        backgroundImageView.image = UIImage(named: imageName)
        backgroundImageView.contentMode = contentMode
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(backgroundImageView)
        sendSubview(toBack: backgroundImageView)
        
        let leadingConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let trailingConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        let topConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: backgroundImageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        
        NSLayoutConstraint.activate([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
    }
}

// MARK: - UIButton Extension
extension UIButton{
    func animateHamburgerMenu(when state: Bool, completion: (()->())? = nil ){
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = state ? CGAffineTransform(rotationAngle: CGFloat.pi / 2) : CGAffineTransform(rotationAngle: CGFloat.pi * 2)
        }) { _ in completion?() }
    }
    
    func animateZoomButton(){
        UIView.animate(withDuration: 0.15, animations: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { _ in
            UIView.animate(withDuration: 0.15, animations: {
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        }
    }
}

// MARK: - UIViewController Extension
extension UIViewController{
    func showAlert(alertTitle title: String = "Alert", alertMessage message: String,
                   okText oktitle: String = "Ok", okHandler handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: oktitle, style: .default, handler: handler))
        present(alert, animated: true, completion: nil)
    }
    func showAlertWithCancel(alertTitle title: String = "Alert", alertMessage message: String,
                   okText oktitle: String = "Ok", cancelText canceltitle: String = "Cancel",
                   cancelHandler cHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                   okHandler handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: oktitle, style: .default, handler: handler))
        alert.addAction(UIAlertAction(title: canceltitle, style: .default, handler: cHandler))
        present(alert, animated: true, completion: nil)
    }
    
    func showActionSheet(alertTitle title: String = "Alert",
                             FirstOptionTitle firstOptionTitle: String,
                             SecondOptionTitle secondOptionTitle: String,
                             ThirdOptionTitle thirdOptionTitle: String,
                             FourthOptionTitle fourthOptionTitle: String?,
                             FirstOptionHandler firstOptionHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                             SecondOptionHandler secondOptionHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                             ThirdOptionHandler thirdOptionHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                             FourthOptionHandler fourthOptionHandler: ((UIAlertAction) -> Swift.Void)? = nil){
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: firstOptionTitle, style: .default, handler: firstOptionHandler))
        alert.addAction(UIAlertAction(title: secondOptionTitle, style: .default, handler: secondOptionHandler))
        alert.addAction(UIAlertAction(title: thirdOptionTitle, style: .default, handler: thirdOptionHandler))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewCell Extension
extension UICollectionViewCell{
    func highlightedWhenTapped(completion: @escaping () -> ()){
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.6
        }) { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 1
            }, completion: { _ in
                completion()
            })
        }
    }
}

// MARK: - UINavigationItem
extension UINavigationItem{
    func setBackButtonTitle(to title: String){
        let back = UIBarButtonItem()
        back.title = title
        self.backBarButtonItem = back
    }
}

// MARK: - UIImage Extension
extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo250KB() -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 500
        
        while imageSizeKB > 500 {
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = UIImagePNGRepresentation(resizedImage)
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 500
        }
        
        return resizingImage
    }
    
    func base64() -> String {
        let resizedImage: UIImage = self.resizedTo250KB()!
        let imageData: NSData = UIImagePNGRepresentation(resizedImage)! as NSData
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
}

// MARK: - String Extension
extension String {
    func stringToUIImage() -> UIImage? {
        guard let dataDecode: NSData = NSData(base64Encoded: self, options:.ignoreUnknownCharacters),
            let image: UIImage = UIImage(data: dataDecode as Data) else {
                return nil
        }
        
        return image
    }
}
