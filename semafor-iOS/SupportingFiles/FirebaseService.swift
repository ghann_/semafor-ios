//
//  File.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/6/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth
import SVProgressHUD

class FirebaseService{
    
    private init(){}
    
    static let shared = FirebaseService()
    
    var currentUser: User? {
        return Auth.auth().currentUser
    }
    
    var currentUid: String? {
        return Auth.auth().currentUser?.uid
    }
    
    func configure(){
        FirebaseApp.configure()
    }
    
    func signUpUSer(withEmail email: String, password: String, handler: @escaping (Error?)->()){
        Auth.auth().createUser(withEmail: email, password: password) { user, err in
            SVProgressHUD.dismiss(completion: {
                guard let error = err else {
                    handler(nil)
                    return
                }
                handler(error)
            })
        }
    }
    
    func signInUser(withEmail email: String, password: String, handler: @escaping (Error?)->()){
        Auth.auth().signIn(withEmail: email, password: password) { user, err in
            SVProgressHUD.dismiss(completion: {
                guard let error = err else {
                    handler(nil)
                    return
                }
                handler(error)
            })
        }
    }
    
    func userForgotPassword(withEmail email: String, handler: @escaping (Error?)->()){
        Auth.auth().sendPasswordReset(withEmail: email) { err in
            SVProgressHUD.dismiss(completion: {
                guard let error = err else {
                    handler(nil)
                    return
                }
                handler(error)
            })
        }
    }
    
    func signOutCurrentUser(){
        do{
           try Auth.auth().signOut()
        }catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    
    func fetchUserProfile(withHandler handler: @escaping (_ User: UserInfo) -> ()){
        SVProgressHUD.show()
        Database.database().reference().child("users/\(currentUid!)").queryOrderedByKey().observe(.value, with: { snapshot in
            
            guard let dict = snapshot.value as? NSDictionary,
                let firstName = dict["firstName"] as? String,
                let lastName  = dict["lastName"] as? String,
                let picture   = dict["picture"] as? String,
                let lat       = dict["lat"] as? Double,
                let lng       = dict["lng"] as? Double
                else{
                    print("Error in userProfile data")
                    return
            }

            let user = UserInfo(firstName: firstName, lastName: lastName, profile: picture.stringToUIImage(), lat: lat, lng: lng)
            SVProgressHUD.dismiss(completion: {
                handler(user)
            })
        })
    }
    
    func saveUserProfile(firstName: String, lastName: String, picture: UIImage, lat: Double, lng: Double, handler: @escaping ()->()){
        let firebaseRef = Database.database().reference()
        
        let userProfile = [
            "firstName"   : firstName,
            "lastName"    : lastName,
            "picture"     : picture.base64(),
            "lat"         : lat,
            "lng"         : lng
            ] as [String : Any]
        
        firebaseRef.child("users/\(currentUid!)").setValue(userProfile)
        
        DispatchQueue.main.async {
            SVProgressHUD.dismiss(completion: {
               handler()
            })
        }
    }
}
