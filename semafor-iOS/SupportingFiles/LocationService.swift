//
//  LocationService.swift
//  semafor-iOS
//
//  Created by unrealBots on 12/10/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class LocationService{
    private init(){}
    
    static let shared = LocationService()
    let locationManager = CLLocationManager()
}

extension CLLocationManager{
    var latitude: CLLocationDegrees {
        get{
            guard let lat = self.location?.coordinate.latitude else {return 0.0}
            return lat
        }
    }
    var longitude: CLLocationDegrees{
        get{
            guard let lng = self.location?.coordinate.longitude else {return 0.0}
            return lng
        }
    }
}
