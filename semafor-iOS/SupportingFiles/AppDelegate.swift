//
//  AppDelegate.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/24/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import UIKit
import UserNotifications
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        configureTabBar()
        configureNavigation()
        configureSVProgressHUD()
        
        FirebaseService.shared.configure()
        
        let vc = (FirebaseService.shared.currentUser != nil) ? UIStoryboard(name: "LeftMenu", bundle: nil) : UIStoryboard(name: "Main", bundle: nil)
        self.window?.rootViewController = vc.instantiateInitialViewController()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted { print("Notification access denied.") }
        }
        
        return true
    }
    
    func configureNavigation(){
        let largeTextAttributes = [NSAttributedStringKey.foregroundColor : Semafor.Colors.defaultWhite,
                                   NSAttributedStringKey.font: UIFont(name: "Raleway-SemiBold", size: 35)!]
        let textAttributes      = [NSAttributedStringKey.foregroundColor : Semafor.Colors.defaultWhite,
                                   NSAttributedStringKey.font: UIFont(name: "Raleway-SemiBold", size: 20)!]
        
        UINavigationBar.appearance().largeTitleTextAttributes = largeTextAttributes
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UINavigationBar.appearance().barTintColor = Semafor.Colors.defaultDarkerBlue
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func configureTabBar(){
        UITabBar.appearance().tintColor = Semafor.Colors.defaultBlue
        UITabBar.appearance().barTintColor = Semafor.Colors.defaultDarkBlue
    }

    func configureSVProgressHUD(){
        SVProgressHUD.setForegroundColor(Semafor.Colors.defaultBlue)
        SVProgressHUD.setBackgroundColor(Semafor.Colors.defaultDarkBlue)
        SVProgressHUD.setFadeInAnimationDuration(0.25)
        SVProgressHUD.setFadeOutAnimationDuration(0.25)
        SVProgressHUD.setRingThickness(3)
    }
    
    func scheduleNotification(dateInfo: DateComponents, title contentTitle: String, body contentBody: String) {
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: false)
        let content = UNMutableNotificationContent()
        content.title = contentTitle
        content.body = contentBody
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error {
                print("Error: \(error)")
            }
        }
    }
}

