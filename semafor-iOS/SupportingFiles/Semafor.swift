//
//  semafor.swift
//  semafor-iOS
//
//  Created by unrealBots on 11/25/17.
//  Copyright © 2017 unrealBots. All rights reserved.
//

import Foundation
import UIKit

struct Semafor{
    static let notification: NotificationCenter = NotificationCenter.default
    static let notificationKey = "com.unrealbots.semafor-iOS"
    
    struct Colors {
        static let defaultBlack      = UIColor(red:0.12, green:0.12, blue:0.12, alpha:1.0) //1e1e1e
        static let defaultBlackAlt   = UIColor(red:0.12, green:0.12, blue:0.12, alpha:0.4) //1e1e1e
        static let defaultWhite      = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0) //ebebeb
        static let defaultBlue       = UIColor(red:0.29, green:0.60, blue:0.84, alpha:1.0) //4b98d6
        static let defaultDarkBlue   = UIColor(red:0.12, green:0.13, blue:0.19, alpha:1.0) //1f2031
        static let defaultDarkerBlue = UIColor(red:0.16, green:0.18, blue:0.25, alpha:1.0) //292D40
    }
    
    enum userStateMain {
        case signIn
        case signUp
    }
    
    enum userStateLogin{
        case fromSignIn
        case fromSignUp
    }
    
    enum userStateChangeProfilePicture{
        case camera
        case library
    }
    
    enum userStateSaveButton {
        case placedHomeAnnotation
        case addFirstName
    }
    
    enum userStateAnnotation{
        case traffic
        case placed
    }
    
    enum stateTrafficLight{
        case red
        case green
    }
    
    enum userStateTrip{
        case onGoing
        case canceled
    }
}
